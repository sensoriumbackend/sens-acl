<?php

namespace Sensorium\SensACL;

use Silber\Bouncer\Database\Role;
use Silber\Bouncer\Database\Models;
use Silber\Bouncer\Database\Scope\Scope as BouncerScope;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Scope extends BouncerScope
{

    /**
     * Append the tenant ID to the given cache key.
     *
     * @param  string  $key
     * @return string
     */
    public function appendToCacheKey($key)
    {
        return is_null($this->scope) ? $key : $key.'-'.$this->scope;
    }

    /**
     * Scope the given model query to the current tenant.
     *
     * @param  \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $table
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function applyToModelQuery($query, $table = null)
    {
        if (is_null($table)) {
            $table = $query->getModel()->getTable();
        }

        if (is_null($this->scope) || $this->onlyScopeRelations) {
            return $this->applyNullScope($query, $table);
        }

        return $this->applyToQuery($query, $table);
    }

    /**
     * Scope the given relationship query to the current tenant.
     *
     * @param  \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $table
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function applyToRelationQuery($query, $table)
    {
        if (is_null($this->scope)) {
            return $this->applyNullScope($query, $table);
        }

        return $this->applyToQuery($query, $table);
    }

    protected function applyNullScope($query, $table)
    {
        return $query->whereNull("{$table}.scope");
    }

    /**
     * Apply the current scope to the given query.
     *
     * This internal method does not check whether
     * the given query needs to be scoped. That
     * is fully the caller's responsibility.
     *
     * @param  \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $table
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    protected function applyToQuery($query, $table)
    {
        return $query->where(function ($query) use ($table) {
            $query->where("{$table}.scope", '=', $this->scope);
                  //->orWhereNull("{$table}.scope"); //uncommented
        });
    }
}
