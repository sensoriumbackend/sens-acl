<?php

namespace Sensorium\SensACL;

use Illuminate\Database\Eloquent\Model;
use Silber\Bouncer\Bouncer;
use Silber\Bouncer\Database\Scope\Scope;

/**
 *   This Wrapper adds handles Bouncer scopes in bouncer calls
 *   Usage:
 *   SensACL::canFor('ability', 'scope', Model::class) => Checks the ability against 'scope'
 *   SensACL::can('ability', Model::class) => Checks the ability against $rootScope
 *
 *   To create roles and abilities, be ware of the scope
 *   Scoped roles and abilities.
 *   1. Use onceTo as callback preferably to avoid unexpected scope behavior
 *   SensACL::scope()->onceTo('scope', function() {
 *       SensACL::allow($this)->everything(); // everything ability will be scoped to 'scope'
 *       SensACL::assign('client')->to($this->user); // role will be assigned for null scope
 *   });
 *
 *   2. Set scope upfront, but be aware that the scope has been changed permanently for the whole execution
 *   SensACL::scope()->to('scope')
 *   SensACL:allow($this)->everything(); // everything ability will be scoped to 'scope'
 *
 *   Set scope when creating and assigning roles and abilities!!
 */

class SensACL extends Bouncer
{
    // This is the default scope for global roles and permissions. 
    // Null scope can be used to get all roles and permissions it serves as 'not defined, so pick all'
    // Other scopes are defined by the BKEY of the model. If set all queries are scoped to that specific BKEY
    protected $rootScope = 'ROOT'; 

	/* Set scope, subsequent calls will be scoped to $scope */
	public function scopeTo($scope) {
	    $this->scope()->to($scope);
	}

    /* Set scope, subsequent calls will be scoped to $rootScope */
    public function scopeToRoot() {
        $this->scope()->to($this->rootScope);
    }

	/* Check $ability for $scope */
	public function canFor($ability, $scope, $args=[]){
		if(is_null($scope)) {
			$this->scopeToRoot();
        }else{
            $this->scopeTo($scope);
        }

	    return parent::can($ability, $args);
	}

	/* Check $ability for $scope */
	public function cannotFor($ability, $scope, $args=[]){
		if(is_null($scope)) {
            $this->scopeToRoot();
        }else{
            $this->scopeTo($scope);
        }

	    return parent::cannot($ability, $args);
	}

	/* Check if $user has $role for $scope */
	public function isFor($role, $user, $scope) {
        if(is_null($scope)) {
            $this->scopeToRoot();
        }else{
            $this->scopeTo($scope);
        }

	    return parent::is($user)->a($role);
	}

	public function scopeReset() {
		$this->scope(new Scope);
	}

	/**
     * Determine if the given ability is allowed.
     * @override: Set scope to null
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     * @return bool
     */
    public function can($ability, $arguments = [])
    {
        $this->scopeToRoot();
        return $this->gate()->allows($ability, $arguments);
    }

    /**
     * Determine if the given ability is allowed.
     * @override: Set scope to null
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     * @return bool
     */
    public function cannot($ability, $arguments = [])
    {
        $this->scopeToRoot();
        return $this->gate()->denies($ability, $arguments);
    }

    /**
     * Start a chain, to check if the given authority has a certain role.
     * @override: Set scope to null to get all roles
     *
     * @param  \Illuminate\Database\Eloquent\Model  $authority
     * @return \Silber\Bouncer\Conductors\ChecksRoles
     */
    public function is(Model $authority)
    {
        $this->scopeTo(null);
        return parent::is($authority);
    }


	/* @override - return own factory to ensure our service is created instead of Bouncer */
	public static function make($user = null)
    {
        return new Factory($user);
    }
}