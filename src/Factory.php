<?php

namespace Sensorium\SensACL;

use Sensorium\SensACL\SensACL;
use Silber\Bouncer\Factory as BouncerFactory;

class Factory extends BouncerFactory
{
	// @override - this function should return SensACL instance not extended Bouncer
	public function create()
    {
        $gate = $this->getGate();
        $guard = $this->getGuard();

        $sacl = (new SensACL($guard))->setGate($gate);

        if ($this->registerAtGate) {
            $guard->registerAt($gate);
        }

        if ($this->registerAtContainer) {
            $sacl->registerClipboardAtContainer();
        }

        return $sacl;
    }
}