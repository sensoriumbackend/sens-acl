<?php

namespace Sensorium\SensACL;

use Illuminate\Support\Facades\Facade;

class SensACLFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return SensACL::class;
    }
}