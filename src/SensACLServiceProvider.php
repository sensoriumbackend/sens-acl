<?php

namespace Sensorium\SensACL;

use Illuminate\Cache\ArrayStore;
use Illuminate\Contracts\Auth\Access\Gate;
use Silber\Bouncer\Bouncer;
use Silber\Bouncer\BouncerServiceProvider;
use Silber\Bouncer\CachedClipboard;

class SensACLServiceProvider extends BouncerServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSensACL();
        $this->registerCommands();
    }

    protected function registerSensACL()
    {
        $acl = SensACL::make()
                ->withClipboard(new CachedClipboard(new ArrayStore))
                ->withGate($this->app->make(Gate::class))
                ->create()
                ->dontCache();

        $acl->scope(new Scope); // Use own scope to support the NULL scope

        $this->app->instance(SensACL::class, $acl);

        /*$this->app->singleton(SensACL::class, function() {
            return SensACL::make()
                ->withClipboard(new CachedClipboard(new ArrayStore))
                ->withGate($this->app->make(Gate::class))
                ->create()
                // don't cache at all!
                ->dontCache();
                
                // use cross-request caching (without cache() caching only takes place during a request) 
                // cache:refresh() needed in case acl rules change
                //->cache(); 
        });*/
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerMorphs();
        $this->setTablePrefix();
        $this->setUserModel();

        if ($this->runningInConsole()) {
            $this->publishMiddleware();
            $this->publishMigrations();
            $this->publishRC5Migrations();
        }
    }

    // Migrations related to Bouncer RC5 release
    protected function publishRC5Migrations()
    {
        if (class_exists('SensACLChangeColumnsPermissionsTable')) {
            return;
        }

        $timestamp = date('Y_m_d_His', time());

        $stub = __DIR__.'/../migrations/change_columns_permissions_table.php';

        $target = $this->app->databasePath().'/migrations/'.$timestamp.'_sens_acl_change_columns_permissions_table.php';

        $this->publishes([$stub => $target], 'sensacl.rc5-migration');
    }

}
