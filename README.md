# Sensorium ACL

This package adds functionality to the Bouncer package specific for Sensorium.
Extra functionality: Better support for the scope usage

### Installing

* Add repository to composer file: 
	"repositories": [
        {
            "type": "vcs",
            "url": "https://chris-swc@bitbucket.org/sensoriumdev/sens-acl.git"
        }
    ],
* composer require sensorium/sens-acl:dev-master
* install bouncer and sens-acl separately if compose stability conflict OR add "minimum-stability": "dev", "prefer-stable": true in the project composer file
* follow usage instructions for Bouncer
* update afterwards: composer update sensorium/sens-acl
* Check bouncer is not discovered (since it is wrapped in this module) ("extra": { "laravel": { "dont-discover": [ "silber/bouncer" ] } )

### RC.5
* Permissions table columns 'entity_id' & 'entity_type' are nullable now. In case of upgrade, change these columns
* Publish migration: php artisan vendor:publish --tag='sensacl.rc5-migration'