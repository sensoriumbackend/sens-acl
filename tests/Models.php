<?php

namespace Sensorium\SensACL\Tests;

use Illuminate\Database\Eloquent\Model;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Model
{
    use HasRolesAndAbilities;

    protected $table = 'users';
    protected $guarded = [];
}

class A extends Model
{

}

class B extends Model
{

}

class C extends Model
{

}