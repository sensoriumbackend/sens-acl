<?php

namespace Sensorium\SensACL\Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Orchestra\Testbench\TestCase;
use Sensorium\SensACL\SensACL;
use Sensorium\SensACL\SensACLServiceProvider;
use Silber\Bouncer\Bouncer;
use Silber\Bouncer\Database\Ability;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Silber\Bouncer\Database\Role;
use Silber\Bouncer\Database\Scope\Scope;
use Silber\Bouncer\Guard;
use Silber\Bouncer\Tests\BaseTestCase;

class SensACLTest extends BaseTestCase
{

	protected function sensACL(Model $authority = null)
    {
        $gate = $this->gate($authority ?: User::create());
        $bouncer = new SensACL((new Guard($this->clipboard))->registerAt($gate));
        $bouncer->scope(new Scope);
        $bouncer->setGate($gate);

        return $bouncer;
    }

    protected function getTablesNames()
    {
        return [
            'users',
            'abilities',
            'roles',
            'assigned_roles',
        ];
    }

    /** @test */
    public function it_can_migrate() 
    {
        foreach ($this->getTablesNames() as $table) {
            $this->assertTrue(Schema::hasTable($table), "The table [$table] not found in the database.");
        }
    } 

    /** @test */
    public function it_can_use_bouncer()
    {
        $user = User::create();
        $sensACL = $this->sensACL($user);

        Ability::create(['name' => 'edit-site']);

        $sensACL->allow($user)->to('do-something');
        $this->assertTrue($sensACL->can('do-something'));
    }

    /** @test */
    public function it_can_check_scoped_abilities()
    {
        $user = User::create();
        $sensACL = $this->sensACL($user);

        $sensACL->scopeTo(1);
        Ability::create(['name' => 'edit-site']);

        $sensACL->allow($user)->to('do-something');
        $this->assertTrue($sensACL->canFor('do-something', 1));
        $this->assertFalse($sensACL->canFor('do-something', 2));

        $this->assertTrue($sensACL->cannotFor('do-something', 2));
    }

    /** @test */
    public function it_can_check_scoped_role()
    {
        $user = User::create();
        $sensACL = $this->sensACL($user);

        $sensACL->scopeTo(1);
        $role = Role::create([
            'name' => 'roleA',
            'title' => 'Client role',
        ]);
        $ability = Ability::create(['name' => 'edit-site']);
        $sensACL->allow($role)->to($ability);
        $sensACL->assign('roleA')->to($user);

        $this->assertTrue($sensACL->canFor('edit-site', 1));
        $this->assertFalse($sensACL->cannotFor('edit-site', 1));
        $this->assertTrue($sensACL->isFor('roleA', $user, 1));
    }

    /** @test */
    public function it_can_check_scoped_and_unscoped_abilities()
    {
        $user = User::create();
        $sensACL = $this->sensACL($user);

        // NULL scope (global) roles and abilities
        $client = $sensACL->role()->firstOrCreate([
            'name' => 'roleB',
            'title' => 'Client',
        ]);
        $sensACL->allow($client)->to('create', A::class);
        $sensACL->allow($client)->to('create', B::class);
        $sensACL->assign('roleB')->to($user);

        // Scoped roles and abilities
        $sensACL->scopeTo(1);
        $sensACL->allow($user)->everything();

        $this->assertTrue($sensACL->is($user)->an('roleB'));
        $this->assertFalse($sensACL->can('edit', User::class));
        $this->assertTrue($sensACL->can('create', A::class));
        $this->assertTrue($sensACL->cannot('read', A::class));
        $this->assertTrue($sensACL->cannot('create', C::class));

        //dump('---scope here---');

        $this->assertTrue($sensACL->canFor('read', 1, A::class));
        $this->assertTrue($sensACL->canFor('read', 1, B::class));
        $this->assertTrue($sensACL->canFor('read', 1, User::class));
        $this->assertTrue($sensACL->canFor('create', 1, C::class));
        //$sensACL->scopeTo(null);
        $this->assertTrue($sensACL->cannot('read', User::class));
    }

    /** @test */
    public function it_can_check_wildcard_forbid_abilities()
    {
        $user = User::create();
        $sensACL = $this->sensACL($user);

        // NULL scope (global) roles and abilities
        $client = $sensACL->role()->firstOrCreate([
            'name' => 'roleB',
            'title' => 'Client',
        ]);
        $sensACL->allow($client)->to('create', A::class);
        $sensACL->allow($client)->to('create', B::class);
        $sensACL->assign('roleB')->to($user);

        // Scoped roles and abilities
        $sensACL->scopeTo(1);
        $scopedOwner = $sensACL->role()->firstOrCreate([
            'name' => 'scoped owner',
            'title' => 'owner',
        ]);
        $sensACL->allow($scopedOwner)->to('*','*');
        // $sensACL->allow($scopedOwner)->toManage('*'); // same as ->to('*','*')
        $sensACL->forbid($scopedOwner)->to('*', A::class);
        $sensACL->assign('scoped owner')->to($user);

        $this->assertTrue($sensACL->is($user)->an('roleB'));
        $this->assertFalse($sensACL->can('edit', User::class));
        $this->assertTrue($sensACL->can('create', A::class));
        $this->assertTrue($sensACL->cannot('read', A::class));
        $this->assertTrue($sensACL->cannot('create', C::class));

        //dump('---scope here---');
        //

        $this->assertTrue($sensACL->canFor('read', 1, B::class));
        $this->assertTrue($sensACL->canFor('create', 1, B::class));
        $this->assertTrue($sensACL->cannotFor('read', 1, A::class));
        $this->assertTrue($sensACL->cannotFor('update', 1, A::class));
        $this->assertTrue($sensACL->canFor('read', 1, User::class));
        $this->assertTrue($sensACL->canFor('create', 1, C::class));
        //$sensACL->scopeTo(null);
        $this->assertTrue($sensACL->cannot('read', User::class));
    }

}
