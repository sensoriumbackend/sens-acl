<?php

namespace Sensorium\SensACL\Tests;

use Illuminate\Cache\ArrayStore;
use Illuminate\Database\Eloquent\Model;
use Sensorium\SensACL\SensACL;
use Silber\Bouncer\CachedClipboard;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Silber\Bouncer\Database\Scope\Scope;
use Silber\Bouncer\Guard;
use Silber\Bouncer\Tests\BaseTestCase;

class CachedClipboardTest extends BaseTestCase
{
    function setUp()
    {
        parent::setUp();

        $this->clipboard = new CachedClipboard(new ArrayStore);
    }

    protected function sensACL(Model $authority = null)
    {
        $gate = $this->gate($authority ?: User::create());
        $bouncer = new SensACL((new Guard($this->clipboard))->registerAt($gate));
        $bouncer->scope(new Scope);
        $bouncer->setGate($gate);

        return $bouncer;
    }

    /** @test */
    public function it_can_check_cached_scoped_and_unscoped_abilities()
    {
        $user = User::create();
        $sensACL = $this->sensACL($user);

        // NULL scope (global) roles and abilities
        $client = $sensACL->role()->firstOrCreate([
            'name' => 'roleB',
            'title' => 'Client',
        ]);
        $sensACL->allow($client)->to('create', A::class);
        $sensACL->allow($client)->to('create', B::class);
        $sensACL->assign('roleB')->to($user);

        // Scoped roles and abilities
        $sensACL->scopeTo(1);
        $sensACL->allow($user)->everything();

        $this->assertTrue($sensACL->is($user)->an('roleB'));
        $this->assertFalse($sensACL->can('edit', User::class));
        $this->assertTrue($sensACL->can('create', A::class));
        $this->assertTrue($sensACL->cannot('read', A::class));
        $this->assertTrue($sensACL->cannot('create', C::class));

        //dump('---scope here---');

        $this->assertTrue($sensACL->canFor('read', 1, A::class));
        $this->assertTrue($sensACL->canFor('read', 1, B::class));
        $this->assertTrue($sensACL->canFor('read', 1, User::class));
        $this->assertTrue($sensACL->canFor('create', 1, C::class));
        //$sensACL->scopeTo(null);
        $this->assertTrue($sensACL->cannot('read', User::class));
    }

}