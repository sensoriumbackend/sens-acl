<?php

use Silber\Bouncer\Database\Models;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SensACLChangeColumnsPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Models::table('permissions'), function (Blueprint $table) {
            $table->integer('entity_id')->unsigned()->nullable()->change();
            $table->string('entity_type')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Models::table('permissions'), function (Blueprint $table) {
            $table->integer('entity_id')->unsigned()->change();
            $table->string('entity_type')->change();
        });
    }
}
